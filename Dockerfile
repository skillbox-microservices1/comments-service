FROM fredrikhgrelland/alpine-jdk11-openssl
WORKDIR /app
COPY build/libs/*.jar ./comments.jar

CMD ["java", "-jar", "comments.jar"]