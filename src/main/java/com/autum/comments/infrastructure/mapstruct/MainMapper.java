package com.autum.comments.infrastructure.mapstruct;

public interface MainMapper<T, E> {
    E map(T t);
}