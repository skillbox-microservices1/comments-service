package com.autum.comments.config;


import com.autum.comments.properties.KafkaProperties;
import lombok.AllArgsConstructor;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.*;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;


@Configuration
@AllArgsConstructor
public class KafkaConfig {

    private KafkaProperties properties;

    @Bean
    public ProducerFactory<String, Object> producerFactory() {
        var configProps = new HashMap<String, Object>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, properties.getBootstrapServers());
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        initSSLConfig(configProps);
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    private void initSSLConfig(HashMap<String, Object> configProps) {
        if (properties.getProtocol() != null) {
            configProps.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, properties.getProtocol());
        }
        if (properties.getMechanism() != null) {
            configProps.put(SaslConfigs.SASL_MECHANISM, properties.getMechanism());
        }
        if (properties.getJaasConfig() != null) {
            configProps.put(SaslConfigs.SASL_JAAS_CONFIG, properties.getJaasConfig());
        }
    }

    @Bean
    public KafkaTemplate<String, Object> kafkaTemplate(ProducerFactory<String, Object> producerFactory) {
        return new KafkaTemplate<>(producerFactory);
    }
}