package com.autum.comments.business.provider.user;

import com.autum.comments.business.provider.ProviderException;

public interface UserProvider {

    UserDto getUser(String accessToken) throws ProviderException;
}