package com.autum.comments.business.provider.publication;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class PublicationDto {

    private String uuid;
    private String userUuid;
    private String title;
    private String description;
    private Long createdAt;
    private String status;
    private List<String> imageUrls;
}