package com.autum.comments.business.provider.statistics;

import com.autum.comments.business.comments.dto.CommentLikeDto;


public interface StatisticsProducer {

    void likeComment(CommentLikeDto dto);
}