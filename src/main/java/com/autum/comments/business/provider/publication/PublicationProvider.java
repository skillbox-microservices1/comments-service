package com.autum.comments.business.provider.publication;

import com.autum.comments.business.provider.ProviderException;


public interface PublicationProvider {

    PublicationDto getPublication(String uuid) throws ProviderException;
}