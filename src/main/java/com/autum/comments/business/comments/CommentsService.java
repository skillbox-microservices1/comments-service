package com.autum.comments.business.comments;


import com.autum.comments.business.comments.dto.CommentDto;
import com.autum.comments.business.comments.dto.CommentListDto;
import org.springframework.data.domain.Pageable;


public interface CommentsService {

    CommentListDto getCommentList(String publicationUuid, Pageable pageable);

    CommentDto create(String publicationUUid, String text, String accessToken);

    void update(String uuid, String text, String accessToken);

    void delete(String uuid, String publicationUuid, String accessToken);

    void delete(String uuid);

    void like(String commentUuid, String accessToken);
}