package com.autum.comments.business.comments.exception;


public class NoPermissionsException extends RuntimeException {

    public NoPermissionsException(String msg) {
        super(msg);
    }
}