package com.autum.comments.business.comments.persistence;

import com.autum.comments.business.comments.dto.CommentDto;
import com.autum.comments.business.comments.dto.CommentListDto;
import org.springframework.data.domain.Pageable;


public interface CommentsRepository {

    CommentDto getByUuid(String uuid);

    CommentDto getByUuidAndUserUuid(String uuid, String userUuid);

    CommentListDto getCommentsByPublicationUuid(String publicationUuid, Pageable pageable);

    boolean isExists(String uuid);

    CommentDto save(CommentDto dto);
}