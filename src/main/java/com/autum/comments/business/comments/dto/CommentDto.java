package com.autum.comments.business.comments.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
@Builder
public class CommentDto {

    private String id;
    private String uuid;
    private String text;
    private String name;
    private String userUuid;
    private String publicationUuid;
    private boolean deleted;
    private LocalDateTime createdAt;
    private LocalDateTime deletedAt;
}