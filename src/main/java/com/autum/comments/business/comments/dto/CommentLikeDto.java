package com.autum.comments.business.comments.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class CommentLikeDto {

    private String objectUuid;
    private String userUuid;
    private final String type = "COMMENT";
    private Long createdAt;
}