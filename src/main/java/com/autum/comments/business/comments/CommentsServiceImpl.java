package com.autum.comments.business.comments;

import com.autum.comments.business.comments.dto.CommentDto;
import com.autum.comments.business.comments.dto.CommentLikeDto;
import com.autum.comments.business.comments.dto.CommentListDto;
import com.autum.comments.business.comments.exception.CommentNotFoundException;
import com.autum.comments.business.comments.exception.NoPermissionsException;
import com.autum.comments.business.comments.persistence.CommentsRepository;
import com.autum.comments.business.provider.publication.PublicationProvider;
import com.autum.comments.business.provider.statistics.StatisticsProducer;
import com.autum.comments.business.provider.user.UserProvider;
import com.autum.comments.utils.DateUtil;
import com.autum.comments.utils.Generator;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class CommentsServiceImpl implements CommentsService {

    private final CommentsRepository commentRepo;
    private final PublicationProvider publicationProvider;
    private final UserProvider userProvider;
    private final StatisticsProducer producer;


    @Override
    public CommentListDto getCommentList(String publicationUuid, Pageable pageable) {
        //TODO проверяем publication?
        return commentRepo.getCommentsByPublicationUuid(publicationUuid, pageable);
    }

    @Override
    public CommentDto create(String publicationUUid, String text, String accessToken) {
        publicationProvider.getPublication(publicationUUid);
        var user = userProvider.getUser(accessToken);
        return commentRepo.save(CommentDto.builder()
                .uuid(Generator.generateUUID())
                .text(text)
                .name(user.getFirstName() + " " + user.getLastName())
                .publicationUuid(publicationUUid)
                .userUuid(user.getUuid())
                .createdAt(DateUtil.getLocalDateTimeUTCNow())
                .build()
        );
    }

    @Override
    public void update(String uuid, String text, String accessToken) {
        var user = userProvider.getUser(accessToken);
        var comment = getNotNullComment(uuid, user.getUuid());
        comment.setText(text);
        commentRepo.save(comment);
    }

    @Override
    public void delete(String commentUuid, String publicationUuid, String accessToken) {
        var user = userProvider.getUser(accessToken);
        var comment = getNotNullComment(commentUuid);

        if (user.getUuid().equals(comment.getUserUuid())) {
            comment.setDeleted(true);
            //TODO setDeletedAt
            commentRepo.save(comment);
            return;
        } else {
            var publication = publicationProvider.getPublication(publicationUuid);
            if (user.getUuid().equals(publication.getUserUuid())) {
                comment.setDeleted(true);
                //TODO setDeletedAt
                commentRepo.save(comment);
                return;
            }
        }
        throw new NoPermissionsException("The comment or publication does not belong to the user");
    }

    @Override
    public void delete(String uuid) {
        var comment = getNotNullComment(uuid);
        comment.setDeleted(true);
        //TODO setDeletedAt
        commentRepo.save(comment);
    }

    @Override
    public void like(String uuid, String accessToken) {
        var user = userProvider.getUser(accessToken);
        if (!commentRepo.isExists(uuid)) {
            throw new CommentNotFoundException();
        }
        producer.likeComment(CommentLikeDto.builder()
                .objectUuid(uuid)
                .userUuid(user.getUuid())
                .createdAt(DateUtil.getTimeNowMillisUTC())
                .build()
        );
    }

    private CommentDto getNotNullComment(String uuid) {
        var comment = commentRepo.getByUuid(uuid);
        if (comment == null) {
            throw new CommentNotFoundException();
        } else {
            return comment;
        }
    }

    private CommentDto getNotNullComment(String uuid, String userUuid) {
        var comment = commentRepo.getByUuidAndUserUuid(uuid, userUuid);
        if (comment == null) {
            throw new CommentNotFoundException();
        } else {
            return comment;
        }
    }
}