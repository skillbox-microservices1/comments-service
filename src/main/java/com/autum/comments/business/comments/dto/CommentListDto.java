package com.autum.comments.business.comments.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@Builder
public class CommentListDto {

    private long totalCount;
    private List<CommentDto> comments;
}