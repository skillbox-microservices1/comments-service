package com.autum.comments.api.model.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CommentResponse {

    @Schema(description = "Public ID")
    private String uuid;
    @Schema(description = "Text")
    private String text;
    @Schema(description = "Author's name")
    private String name;
    @Schema(description = "Public user ID")
    private String userUuid;
    @Schema(description = "Time of comment creation")
    private Long createdAt;

}