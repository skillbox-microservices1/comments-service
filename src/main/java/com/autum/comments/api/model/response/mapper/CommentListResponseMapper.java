package com.autum.comments.api.model.response.mapper;

import com.autum.comments.api.model.response.CommentListResponse;
import com.autum.comments.api.model.response.CommentResponse;
import com.autum.comments.business.comments.dto.CommentDto;
import com.autum.comments.business.comments.dto.CommentListDto;
import com.autum.comments.infrastructure.mapstruct.MainMapper;
import com.autum.comments.infrastructure.mapstruct.converter.TimeConverter;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring")
public interface CommentListResponseMapper extends MainMapper<CommentListDto, CommentListResponse>, TimeConverter {

    @Override
    @Mapping(target = "items", source = "comments")
    CommentListResponse map(CommentListDto dto);

    @Mapping(target = "createdAt", source = "createdAt", qualifiedByName = "LocalDateTimeToLong")
    CommentResponse commentDtoToCommentResponse(CommentDto dto);
}