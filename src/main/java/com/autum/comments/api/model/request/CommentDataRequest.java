package com.autum.comments.api.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CommentDataRequest {

    @Schema(description = "Text")
    private String text;
}