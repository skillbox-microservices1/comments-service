package com.autum.comments.api.model.response.mapper;

import com.autum.comments.api.model.response.CommentResponse;
import com.autum.comments.business.comments.dto.CommentDto;
import com.autum.comments.infrastructure.mapstruct.MainMapper;
import com.autum.comments.infrastructure.mapstruct.converter.TimeConverter;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring")
public interface CommentResponseMapper extends MainMapper<CommentDto, CommentResponse>, TimeConverter {

    @Override
    @Mapping(target = "createdAt", source = "createdAt", qualifiedByName = "LocalDateTimeToLong")
    CommentResponse map(CommentDto dto);
}