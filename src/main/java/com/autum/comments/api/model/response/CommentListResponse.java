package com.autum.comments.api.model.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class CommentListResponse {

    @Schema(description = "The total number of comments in the database")
    private long totalCount;
    @Schema(description = "List of commentaries")
    private List<CommentResponse> items;
}