package com.autum.comments.api.model.response;

import com.autum.comments.api.error.ErrorCode;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;


@Getter
@Setter
public class ErrorResponse {

    @Schema(description = "Error code")
    private ErrorCode errorCode;
    @Schema(description = "Error message")
    private String message;
    @Schema(description = "Additional error Details")
    private Map<String, String> details;

    public ErrorResponse(ErrorCode code, String msg) {
        errorCode = code;
        message = msg;
    }
}