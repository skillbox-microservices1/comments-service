package com.autum.comments.api.controller;

import com.autum.comments.business.comments.CommentsServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import static com.autum.comments.api.util.JwtUtil.ADMIN;
import static org.springframework.http.HttpStatus.OK;


@Tag(name = "ADMIN ROLE API")
@RestController
@Secured(ADMIN)
@AllArgsConstructor
@RequestMapping(value = "/api/v1/admin/comments")
public class AdminRoleController {

    private CommentsServiceImpl service;


    @Operation(summary = "Delete a comment")
    @DeleteMapping("/{uuid}")
    @ResponseStatus(code = OK)
    public void delete(@PathVariable String uuid) {
        service.delete(uuid);
    }
}