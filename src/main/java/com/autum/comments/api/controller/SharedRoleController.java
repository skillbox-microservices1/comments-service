package com.autum.comments.api.controller;

import com.autum.comments.api.model.response.CommentListResponse;
import com.autum.comments.business.comments.CommentsServiceImpl;
import com.autum.comments.infrastructure.mapstruct.Mapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name = "ANONYM OR SHARED ROLE API")
@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/v1/comments")
public class SharedRoleController {

    private CommentsServiceImpl service;
    private Mapper mapper;


    @Operation(summary = "Find city by name")
    @GetMapping("/publication/{uuid}")
    public CommentListResponse getList(@PathVariable String uuid, Pageable pageable) {
        var list = service.getCommentList(uuid, pageable);
        return mapper.map(list, CommentListResponse.class);
    }
}