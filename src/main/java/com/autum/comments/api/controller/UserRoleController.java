package com.autum.comments.api.controller;

import com.autum.comments.api.model.request.CommentDataRequest;
import com.autum.comments.api.model.response.CommentResponse;
import com.autum.comments.business.comments.CommentsService;
import com.autum.comments.infrastructure.mapstruct.Mapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import static com.autum.comments.api.util.JwtUtil.USER;
import static org.springframework.http.HttpStatus.OK;


@Tag(name = "VIEWER ROLE API")
@RestController
@Secured(USER)
@AllArgsConstructor
@RequestMapping(value = "/api/v1/comments")
public class UserRoleController {

    private CommentsService service;
    private Mapper mapper;


    @Operation(summary = "Create a comment")
    @PutMapping("/publication/{pubUuid}/create")
    @ResponseBody
    public CommentResponse create(@RequestBody CommentDataRequest request,
                                  @PathVariable String pubUuid, @AuthenticationPrincipal Jwt jwt) {
        var commentDto = service.create(pubUuid, request.getText(), jwt.getTokenValue());
        return mapper.map(commentDto, CommentResponse.class);
    }

    @Operation(summary = "Update the comment")
    @PostMapping("/{uuid}/update")
    @ResponseStatus(code = OK)
    public void update(@RequestBody CommentDataRequest request,
                       @PathVariable String uuid, @AuthenticationPrincipal Jwt jwt) {
        service.update(uuid, request.getText(), jwt.getTokenValue());
    }

    @Operation(summary = "Delete a comment")
    @DeleteMapping("/publication/{pubUuid}/comment/{uuid}")
    @ResponseStatus(code = OK)
    public void delete(@PathVariable String pubUuid, @PathVariable String uuid, @AuthenticationPrincipal Jwt jwt) {
        service.delete(uuid, pubUuid, jwt.getTokenValue());
    }

    @Operation(summary = "Like the comment")
    @PostMapping("/{uuid}/like")
    @ResponseStatus(code = OK)
    public void like(@PathVariable String uuid, @AuthenticationPrincipal Jwt jwt) {
        service.like(uuid, jwt.getTokenValue());
    }
}