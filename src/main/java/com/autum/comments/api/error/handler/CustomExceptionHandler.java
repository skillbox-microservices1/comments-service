package com.autum.comments.api.error.handler;

import com.autum.comments.api.model.response.ErrorResponse;
import com.autum.comments.business.comments.exception.CommentNotFoundException;
import com.autum.comments.business.comments.exception.NoPermissionsException;
import com.autum.comments.business.provider.ProviderException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Locale;

import static com.autum.comments.api.error.ErrorCode.*;


@Slf4j
@ControllerAdvice
@AllArgsConstructor
public class CustomExceptionHandler {

    private final MessageSource messageSource;


    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(CommentNotFoundException.class)
    public ErrorResponse commentNotFound(CommentNotFoundException e, Locale locale) {
        log.trace("Comment not found", e);
        var code = COMMENT_NOT_FOUND;
        var msg = messageSource.getMessage(code.name(), null, locale);
        return new ErrorResponse(code, msg);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(NoPermissionsException.class)
    public ErrorResponse notPermission(NoPermissionsException e, Locale locale) {
        log.trace("No permission", e);
        var code = ACCESS_DENIED;
        var msg = messageSource.getMessage(code.name(), null, locale);
        return new ErrorResponse(code, msg);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(AccessDeniedException.class)
    public ErrorResponse accessDenied(AccessDeniedException e, Locale locale) {
        log.trace("Access denied", e);
        var code = ACCESS_DENIED;
        var msg = messageSource.getMessage(code.name(), null, locale);
        return new ErrorResponse(code, msg);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Throwable.class)
    public ErrorResponse internalError(Throwable e, Locale locale) {
        log.error("Unexpected error", e);
        var msg = messageSource.getMessage(INTERNAL_ERROR.name(), null, locale);
        return new ErrorResponse(INTERNAL_ERROR, msg);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    public ErrorResponse handleHttpMediaTypeNotAcceptableException(HttpMediaTypeNotAcceptableException e,
                                                                   Locale locale) {
        log.trace(e.getMessage());
        var msg = messageSource.getMessage(INVALID_MEDIA_TYPE.name(), null, locale);
        return new ErrorResponse(INVALID_MEDIA_TYPE, msg);
    }

    @ExceptionHandler(ProviderException.class)
    public ResponseEntity<String> providerException(ProviderException e) {
        log.trace("Provider exception", e);
        return ResponseEntity
                .status(e.getCode())
                .body(e.getBody());
    }
}