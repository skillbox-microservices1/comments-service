package com.autum.comments.api.error;

public enum ErrorCode {
    COMMENT_NOT_FOUND,
    INVALID_MEDIA_TYPE,
    ACCESS_DENIED,
    INTERNAL_ERROR
}
