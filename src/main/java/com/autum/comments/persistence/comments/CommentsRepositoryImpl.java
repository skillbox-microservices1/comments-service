package com.autum.comments.persistence.comments;

import com.autum.comments.business.comments.dto.CommentDto;
import com.autum.comments.business.comments.dto.CommentListDto;
import com.autum.comments.business.comments.persistence.CommentsRepository;
import com.autum.comments.infrastructure.mapstruct.Mapper;
import com.autum.comments.persistence.comments.entity.Comment;
import com.autum.comments.persistence.comments.repodb.CommentsRepositoryDb;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;


@Component
@AllArgsConstructor
public class CommentsRepositoryImpl implements CommentsRepository {

    private Mapper mapper;
    private CommentsRepositoryDb repositoryDb;


    @Override
    public CommentDto getByUuid(String uuid) {
        return repositoryDb.findByUuidAndDeletedIsFalse(uuid)
                .map(comment -> mapper.map(comment, CommentDto.class))
                .orElse(null);
    }

    @Override
    public CommentDto getByUuidAndUserUuid(String uuid, String userUuid) {
        return repositoryDb.findByUuidAndUserUuidAndDeletedIsFalse(uuid, userUuid)
                .map(comment -> mapper.map(comment, CommentDto.class))
                .orElse(null);
    }

    @Override
    public CommentListDto getCommentsByPublicationUuid(String publicationUuid, Pageable pageable) {
        var page = repositoryDb.findAllByPublicationUuidAndDeletedIsFalse(publicationUuid, pageable);
        return CommentListDto.builder()
                .totalCount(page.getTotalElements())
                .comments(page.stream().
                        map(comment -> mapper.map(comment, CommentDto.class))
                        .collect(Collectors.toList())
                )
                .build();
    }

    @Override
    public boolean isExists(String uuid) {
        return repositoryDb.existsByUuidAndDeletedIsFalse(uuid);
    }

    @Override
    public CommentDto save(CommentDto dto) {
        var comment = mapper.map(dto, Comment.class);
        var savedComment = repositoryDb.save(comment);
        return mapper.map(savedComment, CommentDto.class);
    }
}