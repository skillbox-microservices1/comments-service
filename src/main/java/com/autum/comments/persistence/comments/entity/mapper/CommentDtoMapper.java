package com.autum.comments.persistence.comments.entity.mapper;

import com.autum.comments.business.comments.dto.CommentDto;
import com.autum.comments.infrastructure.mapstruct.MainMapper;
import com.autum.comments.persistence.comments.entity.Comment;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface CommentDtoMapper extends MainMapper<Comment, CommentDto> {
}