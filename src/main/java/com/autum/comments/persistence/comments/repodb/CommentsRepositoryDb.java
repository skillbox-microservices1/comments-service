package com.autum.comments.persistence.comments.repodb;

import com.autum.comments.persistence.comments.entity.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface CommentsRepositoryDb extends MongoRepository<Comment, String> {

    boolean existsByUuidAndDeletedIsFalse(String uuid);

    Optional<Comment> findByUuidAndDeletedIsFalse(String uuid);

    Optional<Comment> findByUuidAndUserUuidAndDeletedIsFalse(String uuid, String userUuid);

    Page<Comment> findAllByPublicationUuidAndDeletedIsFalse(String publicationUuid, Pageable pageable);
}