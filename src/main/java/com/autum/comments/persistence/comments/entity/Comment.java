package com.autum.comments.persistence.comments.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;


@Getter
@Setter
@Document("comments")
public class Comment {

    @Id
    private String id;
    private String uuid;
    private String text;
    private String userUuid;
    private String name; //TODO позже пересмотреть
    private String publicationUuid;
    private boolean deleted;
    private LocalDateTime createdAt;
    private LocalDateTime deletedAt;
}