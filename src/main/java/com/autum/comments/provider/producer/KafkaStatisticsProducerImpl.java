package com.autum.comments.provider.producer;

import com.autum.comments.business.comments.dto.CommentLikeDto;
import com.autum.comments.business.provider.statistics.StatisticsProducer;
import com.autum.comments.properties.KafkaProducerProperties;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;


@Component
@AllArgsConstructor
public class KafkaStatisticsProducerImpl implements StatisticsProducer {

    private KafkaTemplate<String, Object> kafkaTemplate;
    private KafkaProducerProperties properties;


    @Override
    public void likeComment(CommentLikeDto dto) {
        kafkaTemplate.send(properties.getCommentLikeTopic(), dto);
    }
}