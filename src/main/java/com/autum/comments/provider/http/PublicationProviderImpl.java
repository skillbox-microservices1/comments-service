package com.autum.comments.provider.http;

import com.autum.comments.business.provider.ProviderException;
import com.autum.comments.business.provider.publication.PublicationProvider;
import com.autum.comments.business.provider.publication.PublicationDto;
import com.autum.comments.properties.PublicationProperties;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
@AllArgsConstructor
public class PublicationProviderImpl implements PublicationProvider {

    private RestTemplate restTemplate;
    private PublicationProperties publicationProperties;
    private Gson gson;


    @Override
    public PublicationDto getPublication(String uuid) {
        var url = publicationProperties.getUrl() + "/api/v1/publications/" + uuid;
        var response = restTemplate.getForEntity(url, String.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return gson.fromJson(response.getBody(), PublicationDto.class);
        } else {
            throw new ProviderException(response.getStatusCode().value(), response.getBody());
        }
    }
}