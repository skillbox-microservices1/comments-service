import com.autum.comments.CommentsApplication;
import com.autum.comments.api.model.response.CommentListResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest(classes = CommentsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SmokeTest extends AbstractTest {

    @LocalServerPort
    private int port;
    private String baseUrl;
    @Autowired
    private TestRestTemplate client;


    @PostConstruct
    public void init() {
        baseUrl = TRANSFER_PROTOCOL.toLowerCase() + "://localhost:" + port + "/api/v1/comments";
    }

    @Test
    void getCommentList() {
        var publicationId = "YYYIREEDS34854FUIO908DSE45NM78QW";
        var url = baseUrl + "/publication/" + publicationId;
        var response = client.getForEntity(url, CommentListResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(0, body.getItems().size());
        assertEquals(0, body.getTotalCount());
    }
}