import com.autum.comments.CommentsApplication;
import com.autum.comments.api.model.request.CommentDataRequest;
import com.autum.comments.api.model.response.CommentListResponse;
import com.autum.comments.api.model.response.CommentResponse;
import com.autum.comments.business.provider.publication.PublicationDto;
import com.autum.comments.business.provider.user.UserDto;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.mockserver.client.MockServerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;


@SpringBootTest(classes = CommentsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RegressTest extends AbstractTest {

    @LocalServerPort
    private int port;
    private String baseUrl;
    @Autowired
    private TestRestTemplate client;

    private final MockServerClient mockServerClient = new MockServerClient(MOCK_HTTP_SERVER.getHost(), MOCK_HTTP_SERVER.getServerPort());

    @PostConstruct
    public void init() {
        baseUrl = AbstractTest.TRANSFER_PROTOCOL.toLowerCase() + "://localhost:" + port + "/api/v1/comments";
    }

    @Test
    void test() {
        //CREATE COMMENT
        var publicationUuid = "NKOPREDS34854FGUIO908DSE45NM78QW";
        var userUuid = "RUFDCV7890FDERE6709DEWDE324VBFDF";
        var username = "autum";

        var accessToken = getAccessToken(username, client);
        var auth = "Bearer " + accessToken;
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", auth);

        var publicationDto = new PublicationDto();
        publicationDto.setUuid(publicationUuid);
        publicationDto.setTitle("Title");
        publicationDto.setDescription("Description");
        publicationDto.setUserUuid(userUuid);
        publicationDto.setStatus("ACTIVE");
        publicationDto.setCreatedAt(System.currentTimeMillis());

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/publications/" + publicationUuid)
                )
                .respond(response()
                        .withBody(new Gson().toJson(publicationDto))
                );

        var userDto = new UserDto();
        userDto.setUuid(userUuid);
        userDto.setFirstName("Nick");
        userDto.setLastName("One");
        userDto.setMiddleName("Wan");
        var city = new UserDto.CityDto();
        city.setUuid("VNEFIOENFIUFNIESU332FHOEIHJFWOIF");
        city.setName("Asgard");
        city.setCountry("Asgard");
        city.setRegion("Valhalla");
        userDto.setCity(city);
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(100L);

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/users/yourself")
                        .withHeader("Authorization", auth)
                )
                .respond(response()
                        .withBody(new Gson().toJson(userDto))
                );

        var text = "First";
        var request = new CommentDataRequest();
        request.setText(text);

        var url = baseUrl + "/publication/" + publicationUuid + "/create";
        var httpEntity = new HttpEntity<>(request, headers);
        var response = client.exchange(url, HttpMethod.PUT, httpEntity, CommentResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        var commentUuid = body.getUuid();
        assertNotNull(commentUuid);
        assertEquals(text, body.getText());
        assertEquals(userUuid, body.getUserUuid());
        assertEquals("Nick One", body.getName());
        assertNotNull(body.getCreatedAt());

        //GET COMMENT
        var getCommentsUrl = baseUrl + "/publication/" + publicationUuid;
        var commentsResponse = client.getForEntity(getCommentsUrl, CommentListResponse.class);
        assertEquals(HttpStatus.OK, commentsResponse.getStatusCode());
        var commentsBody = commentsResponse.getBody();
        assertNotNull(commentsBody);
        assertEquals(1, commentsBody.getTotalCount());
        assertEquals(1, commentsBody.getItems().size());
        var comment = commentsBody.getItems().get(0);
        assertEquals(commentUuid, comment.getUuid());
        assertEquals(text, comment.getText());
        assertEquals(userUuid, comment.getUserUuid());
        assertEquals("Nick One", comment.getName());
        assertNotNull(comment.getCreatedAt());

        //UPDATE COMMENT
        var newText = "Updated text";
        var updateRequest = new CommentDataRequest();
        updateRequest.setText(newText);

        var updateUrl = baseUrl + "/" + commentUuid + "/update";
        var updateHttpEntity = new HttpEntity<>(updateRequest, headers);
        var updateResponse = client.exchange(updateUrl, HttpMethod.POST, updateHttpEntity, Void.class);
        assertEquals(HttpStatus.OK, updateResponse.getStatusCode());

        //GET UPDATED COMMENT
        var updatedCommentsResponse = client.getForEntity(getCommentsUrl, CommentListResponse.class);
        assertEquals(HttpStatus.OK, updatedCommentsResponse.getStatusCode());
        var updatedCommentsBody = updatedCommentsResponse.getBody();
        assertNotNull(updatedCommentsBody);
        assertEquals(1, updatedCommentsBody.getTotalCount());
        assertEquals(1, updatedCommentsBody.getItems().size());
        var updatedComment = updatedCommentsBody.getItems().get(0);
        assertEquals(comment.getUuid(), updatedComment.getUuid());
        assertEquals(newText, updatedComment.getText());
        assertEquals(comment.getUserUuid(), updatedComment.getUserUuid());
        assertEquals(comment.getName(), updatedComment.getName());
        assertEquals(comment.getCreatedAt(), updatedComment.getCreatedAt());

        //LIKE
        var likeUrl = baseUrl + "/" + commentUuid + "/like";
        var likeResponse = client.exchange(likeUrl, HttpMethod.POST, httpEntity, Void.class);
        assertEquals(HttpStatus.OK, likeResponse.getStatusCode());

        //DELETE
        var deleteUrl = baseUrl + "/publication/" + publicationUuid + "/comment/" + commentUuid;
        var deleteResponse = client.exchange(deleteUrl, HttpMethod.DELETE, httpEntity, Void.class);
        assertEquals(HttpStatus.OK, deleteResponse.getStatusCode());

        //CHECK DELETED
        var deletedCommentsResponse = client.getForEntity(getCommentsUrl, CommentListResponse.class);
        assertEquals(HttpStatus.OK, deletedCommentsResponse.getStatusCode());
        var deletedCommentsBody = deletedCommentsResponse.getBody();
        assertNotNull(deletedCommentsBody);
        assertEquals(0, deletedCommentsBody.getTotalCount());
        assertEquals(0, deletedCommentsBody.getItems().size());
    }
}