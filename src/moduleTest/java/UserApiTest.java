import com.autum.comments.CommentsApplication;
import com.autum.comments.api.model.request.CommentDataRequest;
import com.autum.comments.api.model.response.CommentResponse;
import com.autum.comments.business.provider.publication.PublicationDto;
import com.autum.comments.business.provider.user.UserDto;
import com.google.gson.Gson;
import config.UserMongoConfig;
import org.junit.jupiter.api.Test;
import org.mockserver.client.MockServerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;


@SpringBootTest(classes = CommentsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = UserMongoConfig.class)
public class UserApiTest extends AbstractTest {

    private final MockServerClient mockServerClient = new MockServerClient(MOCK_HTTP_SERVER.getHost(), MOCK_HTTP_SERVER.getServerPort());

    @LocalServerPort
    private int port;
    private String baseUrl;
    @Autowired
    private TestRestTemplate client;

    @PostConstruct
    public void init() {
        baseUrl = TRANSFER_PROTOCOL + "://localhost:" + port + "/api/v1/comments";
    }

    @Test
    void createCommentTest() {
        var publicationUuid = "NKOPREDS34854FGUIO908DSE45NM78QW";
        var userUuid = "RUFDCV7890FDERE6709DEWDE324VBFDF";
        var username = "autum";

        var accessToken = getAccessToken(username, client);
        var auth = "Bearer " + accessToken;
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", auth);

        var publicationDto = new PublicationDto();
        publicationDto.setUuid(publicationUuid);
        publicationDto.setTitle("Title");
        publicationDto.setDescription("Description");
        publicationDto.setUserUuid(userUuid);
        publicationDto.setStatus("ACTIVE");
        publicationDto.setCreatedAt(System.currentTimeMillis());

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/publications/" + publicationUuid)
                )
                .respond(response()
                        .withBody(new Gson().toJson(publicationDto))
                );

        var userDto = new UserDto();
        userDto.setUuid(userUuid);
        userDto.setFirstName("Nick");
        userDto.setLastName("One");
        userDto.setMiddleName("Wan");
        var city = new UserDto.CityDto();
        city.setUuid("VNEFIOENFIUFNIESU332FHOEIHJFWOIF");
        city.setName("Asgard");
        city.setCountry("Asgard");
        city.setRegion("Valhalla");
        userDto.setCity(city);
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(100L);

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/users/yourself")
                        .withHeader("Authorization", auth)
                )
                .respond(response()
                        .withBody(new Gson().toJson(userDto))
                );

        var text = "First";
        var request = new CommentDataRequest();
        request.setText(text);

        var url = baseUrl + "/publication/" + publicationUuid + "/create";
        var httpEntity = new HttpEntity<>(request, headers);
        var response = client.exchange(url, HttpMethod.PUT, httpEntity, CommentResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertNotNull(body.getUuid());
        assertEquals(text, body.getText());
        assertEquals(userUuid, body.getUserUuid());
        assertEquals("Nick One", body.getName());
        assertNotNull(body.getCreatedAt());
        //TODO поверка в базе
    }

    @Test
    void updateCommentTest() {
        var commentUuid = "ZZZEW89NVCOPRE9032MKFDER943224MK";
        var username = "autum";

        var accessToken = getAccessToken(username, client);
        var auth = "Bearer " + accessToken;
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", auth);

        var userDto = new UserDto();
        userDto.setUuid("00000EDS34854FGUIO908DSE45NM78QW");
        userDto.setFirstName("Nick");
        userDto.setLastName("One");
        userDto.setMiddleName("Wan");
        var city = new UserDto.CityDto();
        city.setUuid("RUFDCV7890FDERE6709DEWDE324VBFDF");
        city.setName("Asgard");
        city.setCountry("Asgard");
        city.setRegion("Valhalla");
        userDto.setCity(city);
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(100L);

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/users/yourself")
                        .withHeader("Authorization", auth)
                )
                .respond(response()
                        .withBody(new Gson().toJson(userDto))
                );

        var text = "Updated text";
        var request = new CommentDataRequest();
        request.setText(text);

        var url = baseUrl + "/" + commentUuid + "/update";
        var httpEntity = new HttpEntity<>(request, headers);
        var response = client.exchange(url, HttpMethod.POST, httpEntity, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        //TODO поверка в базе
    }

    @Test
    void deleteCommentTest() {
        var commentUuid = "FDDEW89NVCOPRE9032MKFDER943224MK";
        var publicationUuid = "MNOPDSEW9087633FEFEWFWE3232FER43";
        var username = "autum";

        var accessToken = getAccessToken(username, client);
        var auth = "Bearer " + accessToken;
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", auth);

        var userDto = new UserDto();
        userDto.setUuid("00000EDS34854FGUIO908DSE45NM78QW");
        userDto.setFirstName("Nick");
        userDto.setLastName("One");
        userDto.setMiddleName("Wan");
        var city = new UserDto.CityDto();
        city.setUuid("RUFDCV7890FDERE6709DEWDE324VBFDF");
        city.setName("Asgard");
        city.setCountry("Asgard");
        city.setRegion("Valhalla");
        userDto.setCity(city);
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(100L);

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/users/yourself")
                        .withHeader("Authorization", auth)
                )
                .respond(response()
                        .withBody(new Gson().toJson(userDto))
                );

        var url = baseUrl + "/publication/" + publicationUuid + "/comment/" + commentUuid;
        var httpEntity = new HttpEntity<>(null, headers);
        var response = client.exchange(url, HttpMethod.DELETE, httpEntity, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        //TODO поверка в базе
    }

    @Test
    void likeCommentTest() {
        var commentUuid = "OPDEW89NVCOPRE9032MKFDER943224MK";
        var username = "autum";

        var accessToken = getAccessToken(username, client);
        var auth = "Bearer " + accessToken;
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", auth);

        var userDto = new UserDto();
        userDto.setUuid("00000EDS34854FGUIO908DSE45NM78QW");
        userDto.setFirstName("Nick");
        userDto.setLastName("One");
        userDto.setMiddleName("Wan");
        var city = new UserDto.CityDto();
        city.setUuid("RUFDCV7890FDERE6709DEWDE324VBFDF");
        city.setName("Asgard");
        city.setCountry("Asgard");
        city.setRegion("Valhalla");
        userDto.setCity(city);
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(100L);

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/users/yourself")
                        .withHeader("Authorization", auth)
                )
                .respond(response()
                        .withBody(new Gson().toJson(userDto))
                );

        var url = baseUrl + "/" + commentUuid + "/like";
        var httpEntity = new HttpEntity<>(null, headers);
        var response = client.exchange(url, HttpMethod.POST, httpEntity, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}