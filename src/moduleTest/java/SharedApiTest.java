import com.autum.comments.CommentsApplication;
import com.autum.comments.api.model.response.CommentListResponse;
import config.SharedMongoConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest(classes = CommentsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = SharedMongoConfig.class)
public class SharedApiTest extends AbstractTest {

    @LocalServerPort
    private int port;
    private String baseUrl;
    @Autowired
    private TestRestTemplate client;


    @PostConstruct
    public void init() {
        baseUrl = TRANSFER_PROTOCOL.toLowerCase() + "://localhost:" + port + "/api/v1/comments";
    }

    @Test
    void getCommentListTest() {
        var publicationUuid = "MNOPDSEW9087633FEFEWFWE3232FER43";
        var url = baseUrl + "/publication/" + publicationUuid;
        var response = client.getForEntity(url, CommentListResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(2, body.getTotalCount());
        assertEquals(2, body.getItems().size());
        var comment = body.getItems().get(0);
        assertEquals("OPDEW89NVCOPRE9032MKFDER943224MK", comment.getUuid());
        assertEquals("Test one", comment.getText());
        assertEquals("00000EDS34854FGUIO908DSE45NM78QW", comment.getUserUuid());
        assertEquals("John Doe", comment.getName());
        assertEquals(1720912262345L, comment.getCreatedAt());

        var comment2 = body.getItems().get(1);
        assertEquals("FDDEW89NVCOPRE9032MKFDER943224MK", comment2.getUuid());
        assertEquals("Test delete", comment2.getText());
        assertEquals("00000EDS34854FGUIO908DSE45NM78QW", comment2.getUserUuid());
        assertEquals("John Doe", comment2.getName());
        assertEquals(1720912262345L, comment2.getCreatedAt());
    }

    @Test
    void getCommentList_emptyTest() {
        var publicationId = "YYYIREEDS34854FUIO908DSE45NM78QW";
        var url = baseUrl + "/publication/" + publicationId;
        var response = client.getForEntity(url, CommentListResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(0, body.getItems().size());
        assertEquals(0, body.getTotalCount());
    }
}