import com.autum.comments.CommentsApplication;
import config.AdminMongoConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest(classes = CommentsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = AdminMongoConfig.class)
public class AdminApiTest extends AbstractTest {

    @LocalServerPort
    private int port;
    private String baseUrl;
    @Autowired
    private TestRestTemplate client;


    @PostConstruct
    public void init() {
        baseUrl = TRANSFER_PROTOCOL + "://localhost:" + port + "/api/v1/admin/comments";
    }

    @Test
    void deleteTest() {
        var commentUuid = "OPDEW89NVCOPRE9032MKFDER943224MK";
        var username = "admin";

        var accessToken = getAccessToken(username, client);
        var auth = "Bearer " + accessToken;
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", auth);

        var url = baseUrl + "/" + commentUuid;
        var httpEntity = new HttpEntity<>(null, headers);
        var response = client.exchange(url, HttpMethod.DELETE, httpEntity, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        //TODO поверка в базе
    }
}