package mapper;

import com.autum.comments.business.provider.publication.PublicationDto;
import com.autum.comments.infrastructure.mapstruct.Mapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;


class MapperTest {

    @Test
    public void null_result() {
        var mapper = new Mapper(null);
        var result = mapper.map(null, PublicationDto.class);
        assertNull(result);
    }
}