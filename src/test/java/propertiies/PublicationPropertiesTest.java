package propertiies;

import com.autum.comments.properties.PublicationProperties;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class PublicationPropertiesTest {

    @Test
    void test() {
        var url = "http://autum.com/";

        var pub = new PublicationProperties();
        pub.setUrl(url);

        assertEquals(url, pub.getUrl());
    }
}