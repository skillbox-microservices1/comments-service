package propertiies;

import com.autum.comments.properties.KafkaProducerProperties;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class KafkaProducerPropertiesTest {

    @Test
    void test() {
        var topic = "events";

        var pub = new KafkaProducerProperties();
        pub.setCommentLikeTopic(topic);

        assertEquals(topic, pub.getCommentLikeTopic());
    }
}