package propertiies;

import com.autum.comments.properties.UserProperties;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class UserPropertiesTest {

    @Test
    void test() {
        var url = "http://autum.com/";

        var pub = new UserProperties();
        pub.setUrl(url);

        assertEquals(url, pub.getUrl());
    }
}