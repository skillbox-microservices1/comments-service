import com.autum.comments.utils.DateUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static java.time.ZoneOffset.UTC;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class DateUtilTest {

    @Test
    void getMillisFromLocalDateTimeTest() {
        var time = LocalDateTime.now();
        var expected = time.toInstant(UTC).toEpochMilli();
        var result = DateUtil.getMillisFromLocalDateTime(time);
        Assertions.assertEquals(expected, result);
    }

    @Test
    void getLocalDateTimeUTCNowTest() {
        var result = DateUtil.getLocalDateTimeUTCNow();
        assertNotNull(result);
    }

    @Test
    void getLocalDateTimeUTCTest() {
        var millis = System.currentTimeMillis();
        var expected = Instant.ofEpochMilli(millis)
                .atZone(ZoneId.of("UTC"))
                .toLocalDateTime();
        var result = DateUtil.getLocalDateTimeUTC(millis);
        assertEquals(expected, result);
    }
}