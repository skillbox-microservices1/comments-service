package provider.producer;

import com.autum.comments.business.comments.dto.CommentLikeDto;
import com.autum.comments.properties.KafkaProducerProperties;
import com.autum.comments.provider.producer.KafkaStatisticsProducerImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.AsyncResult;

import static org.mockito.Mockito.*;


public class KafkaStatisticsProducerTest {

    @Test
    void likeComment() {
        var topic = "events";

        var properties = Mockito.mock(KafkaProducerProperties.class);
        var template = Mockito.mock(KafkaTemplate.class);

        var producer = new KafkaStatisticsProducerImpl(template, properties);

        var dto = CommentLikeDto.builder()
                .userUuid("IOIFHWEIOHFWIEJFHJIWE324324FEWGF")
                .objectUuid("1OIFHOOIOHFWIEJFHJIWE324324FEWG4")
                .createdAt(System.currentTimeMillis())
                .build();

        when(properties.getCommentLikeTopic()).thenReturn(topic);
        when(template.send(topic, dto)).thenReturn(new AsyncResult(new Object()));

        producer.likeComment(dto);

        verify(properties, times(1)).getCommentLikeTopic();
        verify(template, times(1)).send(topic, dto);
    }
}