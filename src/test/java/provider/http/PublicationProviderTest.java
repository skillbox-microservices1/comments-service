package provider.http;

import com.autum.comments.business.provider.ProviderException;
import com.autum.comments.business.provider.publication.PublicationDto;
import com.autum.comments.properties.PublicationProperties;
import com.autum.comments.provider.http.PublicationProviderImpl;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;


public class PublicationProviderTest {

    @Test
    void getPublications() {
        var uuid = "VNBEOSONOEW23423IFHOIWEJF2332432";
        var url = "http://localhost:8080";

        var publicationProperties = mock(PublicationProperties.class);
        var restTemplate = mock(RestTemplate.class);
        var gson = new Gson();

        var provider = new PublicationProviderImpl(restTemplate, publicationProperties, gson);

        var publication = new PublicationDto();
        publication.setUuid(uuid);
        publication.setUserUuid("OJSBFOIWEHJFWOIEFOWEFJWEO3223432");
        publication.setTitle("Test title");
        publication.setDescription("Test text");
        publication.setCreatedAt(System.currentTimeMillis());
        publication.setStatus("ACTIVE");
        publication.setImageUrls(List.of("http://localhost:8045/img/HIHEIOFIEHSFS.jpg"));

        when(publicationProperties.getUrl()).thenReturn(url);

        var responseEntity = new ResponseEntity<>(gson.toJson(publication), HttpStatus.OK);

        when(restTemplate.getForEntity(eq(url + "/api/v1/publications/" + uuid), eq(String.class)))
                .thenReturn(responseEntity);

        var result = provider.getPublication(uuid);
        assertEquals(publication.getUuid(), result.getUuid());
        assertEquals(publication.getTitle(), result.getTitle());
        assertEquals(publication.getDescription(), result.getDescription());
        assertEquals(publication.getCreatedAt(), result.getCreatedAt());
        assertEquals(publication.getStatus(), result.getStatus());
        assertEquals(publication.getImageUrls(), result.getImageUrls());

        verify(publicationProperties, times(1)).getUrl();
        verify(restTemplate, times(1)).getForEntity(eq(url + "/api/v1/publications/" + uuid), eq(String.class));
    }

    @Test
    void getPublications_throwsProviderException() {
        var uuid = "VNBEOSONOEW23423IFHOIWEJF2332432";
        var url = "http://localhost:8080";

        var publicationProperties = mock(PublicationProperties.class);
        var restTemplate = mock(RestTemplate.class);
        var gson = new Gson();

        when(publicationProperties.getUrl()).thenReturn(url);

        var errorBody = "Error message";
        var responseEntity = new ResponseEntity<>(errorBody, HttpStatus.BAD_REQUEST);

        when(restTemplate.getForEntity(eq(url + "/api/v1/publications/" + uuid), eq(String.class)))
                .thenReturn(responseEntity);

        var provider = new PublicationProviderImpl(restTemplate, publicationProperties, gson);

        try {
            provider.getPublication(uuid);
        } catch (ProviderException e) {
            assertEquals(HttpStatus.BAD_REQUEST.value(), e.getCode());
            assertEquals(errorBody, e.getBody());
        }

        verify(publicationProperties, times(1)).getUrl();
        verify(restTemplate, times(1)).getForEntity(eq(url + "/api/v1/publications/" + uuid), eq(String.class));
    }
}