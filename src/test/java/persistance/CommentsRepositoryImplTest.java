package persistance;

import com.autum.comments.business.comments.dto.CommentDto;
import com.autum.comments.infrastructure.mapstruct.Mapper;
import com.autum.comments.persistence.comments.CommentsRepositoryImpl;
import com.autum.comments.persistence.comments.entity.Comment;
import com.autum.comments.persistence.comments.repodb.CommentsRepositoryDb;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class CommentsRepositoryImplTest {

    @Test
    void getByUuid() {
        var commentUuid = "TIFDER4342TOPS4DFRE987HBNF899443";

        var repoDb = Mockito.mock(CommentsRepositoryDb.class);
        var mapper = Mockito.mock(Mapper.class);
        var repository = new CommentsRepositoryImpl(mapper, repoDb);

        var comment = new Comment();
        comment.setId("1");
        comment.setUuid("HFOIEWFJOWEIFJEOIJ3232OFJOWIEJFO");
        comment.setUserUuid("NVJENOIRFIO2132ERWFORJIFJOI23223");
        comment.setText("test");
        comment.setDeleted(false);
        comment.setPublicationUuid("22FDER4342TOPS4DFRE987HBNF899488");
        comment.setCreatedAt(LocalDateTime.now());
        comment.setDeletedAt(null);
        comment.setName("Jon Joe");

        when(repoDb.findByUuidAndDeletedIsFalse(commentUuid)).thenReturn(Optional.of(comment));

        var commentDto = CommentDto.builder()
                .id(comment.getId())
                .uuid(comment.getUuid())
                .userUuid(comment.getUserUuid())
                .text(comment.getText())
                .deleted(comment.isDeleted())
                .publicationUuid(comment.getPublicationUuid())
                .createdAt(comment.getCreatedAt())
                .deletedAt(comment.getDeletedAt())
                .name(comment.getName())
                .build();

        when(mapper.map(comment, CommentDto.class)).thenReturn(commentDto);

        var result = repository.getByUuid(commentUuid);

        assertEquals(commentDto, result);

        verify(repoDb, times(1)).findByUuidAndDeletedIsFalse(commentUuid);
        verify(mapper, times(1)).map(comment, CommentDto.class);
    }

    @Test
    void getByUuid_empty() {
        var commentUuid = "TIFDER4342TOPS4DFRE987HBNF899443";

        var repoDb = Mockito.mock(CommentsRepositoryDb.class);
        var mapper = Mockito.mock(Mapper.class);
        var repository = new CommentsRepositoryImpl(mapper, repoDb);

        when(repoDb.findByUuidAndDeletedIsFalse(commentUuid)).thenReturn(Optional.empty());

        var result = repository.getByUuid(commentUuid);

        assertNull(result);

        verify(repoDb, times(1)).findByUuidAndDeletedIsFalse(commentUuid);
    }

    @Test
    void getByUuidAndUserUuid() {
        var commentUuid = "TIFDER4342TOPS4DFRE987HBNF899443";
        var userUuid = "11FDER4342TOPS4DFRE987HBNF899422";

        var repoDb = Mockito.mock(CommentsRepositoryDb.class);
        var mapper = Mockito.mock(Mapper.class);
        var repository = new CommentsRepositoryImpl(mapper, repoDb);

        var comment = new Comment();
        comment.setId("1");
        comment.setUuid(commentUuid);
        comment.setUserUuid(userUuid);
        comment.setText("test");
        comment.setDeleted(false);
        comment.setPublicationUuid("22FDER4342TOPS4DFRE987HBNF899488");
        comment.setCreatedAt(LocalDateTime.now());
        comment.setDeletedAt(null);
        comment.setName("Jon Joe");

        when(repoDb.findByUuidAndUserUuidAndDeletedIsFalse(commentUuid, userUuid)).thenReturn(Optional.of(comment));

        var commentDto = CommentDto.builder()
                .id(comment.getId())
                .uuid(comment.getUuid())
                .userUuid(comment.getUserUuid())
                .text(comment.getText())
                .deleted(comment.isDeleted())
                .publicationUuid(comment.getPublicationUuid())
                .createdAt(comment.getCreatedAt())
                .deletedAt(comment.getDeletedAt())
                .name(comment.getName())
                .build();

        when(mapper.map(comment, CommentDto.class)).thenReturn(commentDto);

        var result = repository.getByUuidAndUserUuid(commentUuid, userUuid);

        assertEquals(commentDto, result);

        verify(repoDb, times(1)).findByUuidAndUserUuidAndDeletedIsFalse(commentUuid, userUuid);
        verify(mapper, times(1)).map(comment, CommentDto.class);
    }

    @Test
    void getByUuidAndUserUuid_empty() {
        var commentUuid = "TIFDER4342TOPS4DFRE987HBNF899443";
        var userUuid = "11FDER4342TOPS4DFRE987HBNF899422";

        var repoDb = Mockito.mock(CommentsRepositoryDb.class);
        var mapper = Mockito.mock(Mapper.class);
        var repository = new CommentsRepositoryImpl(mapper, repoDb);

        when(repoDb.findByUuidAndUserUuidAndDeletedIsFalse(commentUuid, userUuid)).thenReturn(Optional.empty());

        var result = repository.getByUuidAndUserUuid(commentUuid, userUuid);

        assertNull(result);

        verify(repoDb, times(1)).findByUuidAndUserUuidAndDeletedIsFalse(commentUuid, userUuid);
    }

    @Test
    void getCommentsByPublicationUuid() {
        var pubUuid = "TIFDER4342TOPS4DFRE987HBNF899443";
        var pageable = Pageable.ofSize(10);

        var repoDb = Mockito.mock(CommentsRepositoryDb.class);
        var mapper = Mockito.mock(Mapper.class);
        var repository = new CommentsRepositoryImpl(mapper, repoDb);

        var comment1 = new Comment();
        comment1.setId("1");
        comment1.setUuid("IJOWEIFWEJOFJWEOIWEVWEIO33423432");
        comment1.setUserUuid("555WEIFWEJOFJWEOIWEVWEIO33423555");
        comment1.setText("test 1");
        comment1.setDeleted(false);
        comment1.setPublicationUuid(pubUuid);
        comment1.setCreatedAt(LocalDateTime.now());
        comment1.setDeletedAt(null);
        comment1.setName("Jon Joe");

        var comment2 = new Comment();
        comment2.setId("2");
        comment2.setUuid("333WEIFWEJOFJWEOIWEVWEIO33423000");
        comment2.setUserUuid("444WEIFWEJOFJWEOIWEVWEIO33423444");
        comment2.setText("test 2");
        comment2.setDeleted(false);
        comment2.setPublicationUuid(pubUuid);
        comment2.setCreatedAt(LocalDateTime.now());
        comment2.setDeletedAt(null);
        comment2.setName("Jon Joe");

        var page = new PageImpl<>(List.of(comment1, comment2), pageable, 2);

        when(repoDb.findAllByPublicationUuidAndDeletedIsFalse(pubUuid, pageable)).thenReturn(page);

        var commentDto1 = CommentDto.builder().build();

        var commentDto2 = CommentDto.builder().build();

        when(mapper.map(comment1, CommentDto.class)).thenReturn(commentDto1);
        when(mapper.map(comment2, CommentDto.class)).thenReturn(commentDto2);

        var result = repository.getCommentsByPublicationUuid(pubUuid, pageable);

        assertEquals(2, result.getTotalCount());
        assertNotNull(result.getComments());
        assertEquals(2, result.getComments().size());

        var commentResult = result.getComments().get(0);
        assertEquals(commentDto1.getUuid(), commentResult.getUuid());
        assertEquals(commentDto1.getText(), commentResult.getText());
        assertEquals(commentDto1.getName(), commentResult.getName());
        assertEquals(commentDto1.getUserUuid(), commentResult.getUserUuid());
        assertEquals(commentDto1.getCreatedAt(), commentResult.getCreatedAt());

        var commentResult1 = result.getComments().get(1);
        assertEquals(commentDto2.getUuid(), commentResult1.getUuid());
        assertEquals(commentDto2.getText(), commentResult1.getText());
        assertEquals(commentDto2.getName(), commentResult1.getName());
        assertEquals(commentDto2.getUserUuid(), commentResult1.getUserUuid());
        assertEquals(commentDto2.getCreatedAt(), commentResult1.getCreatedAt());

        verify(repoDb, times(1)).findAllByPublicationUuidAndDeletedIsFalse(pubUuid, pageable);
        verify(mapper, times(1)).map(comment1, CommentDto.class);
        verify(mapper, times(1)).map(comment2, CommentDto.class);
    }

    @Test
    void isExists_true() {
        var commentUuid = "TIFDER4342TOPS4DFRE987HBNF899443";

        var repoDb = Mockito.mock(CommentsRepositoryDb.class);
        var repository = new CommentsRepositoryImpl(null, repoDb);

        when(repoDb.existsByUuidAndDeletedIsFalse(commentUuid)).thenReturn(true);

        var result = repository.isExists(commentUuid);
        assertTrue(result);

        verify(repoDb, times(1)).existsByUuidAndDeletedIsFalse(commentUuid);
    }

    @Test
    void isExists_false() {
        var commentUuid = "TIFDER4342TOPS4DFRE987HBNF899443";

        var repoDb = Mockito.mock(CommentsRepositoryDb.class);
        var repository = new CommentsRepositoryImpl(null, repoDb);

        when(repoDb.existsByUuidAndDeletedIsFalse(commentUuid)).thenReturn(false);

        var result = repository.isExists(commentUuid);
        assertFalse(result);

        verify(repoDb, times(1)).existsByUuidAndDeletedIsFalse(commentUuid);
    }

    @Test
    void save() {

        var repoDb = Mockito.mock(CommentsRepositoryDb.class);
        var mapper = Mockito.mock(Mapper.class);
        var repository = new CommentsRepositoryImpl(mapper, repoDb);

        var commentDto = CommentDto.builder()
                .uuid("HFOIEWFJOWEIFJEOIJ3232OFJOWIEJFO")
                .userUuid("NVJENOIRFIO2132ERWFORJIFJOI23223")
                .text("test text")
                .deleted(false)
                .publicationUuid("VBNCIEHFWOIEFHWOEIH2332EWWWWFG23")
                .createdAt(LocalDateTime.now())
                .deletedAt(null)
                .name("Jon Joe")
                .build();

        var comment = new Comment();
        comment.setUuid(commentDto.getUuid());
        comment.setUserUuid(commentDto.getUserUuid());
        comment.setText(commentDto.getText());
        comment.setDeleted(commentDto.isDeleted());
        comment.setPublicationUuid(commentDto.getPublicationUuid());
        comment.setCreatedAt(commentDto.getCreatedAt());
        comment.setDeletedAt(commentDto.getDeletedAt());
        comment.setName(commentDto.getName());

        when(mapper.map(commentDto, Comment.class)).thenReturn(comment);

        var savedComment = new Comment();
        savedComment.setId("1");
        savedComment.setUuid(commentDto.getUuid());
        savedComment.setUserUuid(commentDto.getUserUuid());
        savedComment.setText(commentDto.getText());
        savedComment.setDeleted(commentDto.isDeleted());
        savedComment.setPublicationUuid(commentDto.getPublicationUuid());
        savedComment.setCreatedAt(commentDto.getCreatedAt());
        savedComment.setDeletedAt(commentDto.getDeletedAt());
        savedComment.setName(commentDto.getName());

        when(repoDb.save(comment)).thenReturn(savedComment);

        var savedCommentDto = CommentDto.builder()
                .id(savedComment.getId())
                .uuid(savedComment.getUuid())
                .userUuid(savedComment.getUserUuid())
                .text(savedComment.getText())
                .deleted(savedComment.isDeleted())
                .publicationUuid(savedComment.getPublicationUuid())
                .createdAt(savedComment.getCreatedAt())
                .deletedAt(savedComment.getDeletedAt())
                .name(savedComment.getName())
                .build();

        when(mapper.map(savedComment, CommentDto.class)).thenReturn(savedCommentDto);

        var result = repository.save(commentDto);
        assertEquals(savedCommentDto, result);

        verify(repoDb, times(1)).save(comment);
        verify(mapper, times(1)).map(commentDto, Comment.class);
        verify(mapper, times(1)).map(savedComment, CommentDto.class);
    }
}