package business;

import com.autum.comments.business.comments.CommentsServiceImpl;
import com.autum.comments.business.comments.dto.CommentDto;
import com.autum.comments.business.comments.dto.CommentLikeDto;
import com.autum.comments.business.comments.dto.CommentListDto;
import com.autum.comments.business.comments.exception.CommentNotFoundException;
import com.autum.comments.business.comments.exception.NoPermissionsException;
import com.autum.comments.business.comments.persistence.CommentsRepository;
import com.autum.comments.business.provider.publication.PublicationDto;
import com.autum.comments.business.provider.publication.PublicationProvider;
import com.autum.comments.business.provider.statistics.StatisticsProducer;
import com.autum.comments.business.provider.user.UserDto;
import com.autum.comments.business.provider.user.UserProvider;
import com.autum.comments.utils.DateUtil;
import com.autum.comments.utils.Generator;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


public class CommentsServiceTest {

    @Test
    void getCommentList() {
        var pubUuid = "OPVD34FERRE32233242EFHOIEWJFWIOE";
        var pageable = Pageable.ofSize(10);

        var commentsRepo = Mockito.mock(CommentsRepository.class);

        var service = new CommentsServiceImpl(commentsRepo, null, null, null);

        var commentDto = CommentDto.builder()
                .id("1")
                .uuid("OPMNVFDRTEOWWE2332MOEFOI332VFGTR")
                .text("Test comment")
                .name("Aro Ani")
                .userUuid("TOPDSWEREWEFWEFWEF2323432EFWEFWE")
                .publicationUuid(pubUuid)
                .deleted(false)
                .createdAt(LocalDateTime.now())
                .deletedAt(null)
                .build();

        var commentListDto = CommentListDto.builder()
                .totalCount(1)
                .comments(List.of(commentDto))
                .build();

        when(commentsRepo.getCommentsByPublicationUuid(pubUuid, pageable)).thenReturn(commentListDto);

        var result = service.getCommentList(pubUuid, pageable);
        assertEquals(commentListDto, result);

        verify(commentsRepo, times(1)).getCommentsByPublicationUuid(pubUuid, pageable);
    }

    @Test
    void createTest() {
        var publicationUUid = "OPVD34FERRE32233242EFHOIEWJFWIOE";
        var text = "Text text";
        var accessToken = "eerfergerfewferge";
        var userUuid = "111134FERRE32233242EFHOIEWJFWIOE";

        var commentUuid = "OJVIFJHWEPOFJWEFPOEJ23432POJFOEP";
        var time = LocalDateTime.now();

        var userProvider = Mockito.mock(UserProvider.class);
        var publicationProvider = Mockito.mock(PublicationProvider.class);
        var commentRepo = Mockito.mock(CommentsRepository.class);

        var service = new CommentsServiceImpl(commentRepo, publicationProvider, userProvider, null);

        var publication = new PublicationDto();
        publication.setUuid(publicationUUid);
        publication.setUserUuid("OJSBFOIWEHJFWOIEFOWEFJWEO3223432");
        publication.setTitle("Test title");
        publication.setDescription("Test text");
        publication.setCreatedAt(System.currentTimeMillis());
        publication.setStatus("ACTIVE");
        publication.setImageUrls(List.of("http://localhost:8045/img/HIHEIOFIEHSFS.jpg"));

        when(publicationProvider.getPublication(publicationUUid)).thenReturn(publication);

        var userDto = new UserDto();
        userDto.setUuid(userUuid);
        userDto.setFirstName("Nick");
        userDto.setLastName("Men");
        userDto.setMiddleName("Forever");
        var city = new UserDto.CityDto();
        city.setUuid("BOWBWEIFNWEFNWEOWJERIWEJROOEREWR");
        city.setName("Asgard");
        city.setRegion("Sky");
        city.setCountry("Valhalla");
        userDto.setCity(city);
        var skillDto = new UserDto.SkillDto();
        skillDto.setName("java");
        skillDto.setUuid("OIEBNFIWPEFNWEOIFNWEI2323434FGTR");
        skillDto.setLevel(10);
        userDto.setSkills(List.of(skillDto));
        userDto.setAvatarUrl("http://avatar.url");
        userDto.setAbout("one one");
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(50L);

        when(userProvider.getUser(accessToken)).thenReturn(userDto);

        var savedCommentDto = CommentDto.builder()
                .id("1")
                .uuid(commentUuid)
                .text(text)
                .name(userDto.getFirstName() + " " + userDto.getLastName())
                .publicationUuid(publicationUUid)
                .userUuid(userDto.getUuid())
                .createdAt(time)
                .build();

        when(commentRepo.save(any(CommentDto.class))).thenReturn(savedCommentDto);

        try (MockedStatic<DateUtil> dateUtil = Mockito.mockStatic(DateUtil.class)) {
            dateUtil.when(DateUtil::getLocalDateTimeUTCNow).thenReturn(time);
            try (MockedStatic<Generator> generator = Mockito.mockStatic(Generator.class)) {
                generator.when(Generator::generateUUID).thenReturn(commentUuid);
                var result = service.create(publicationUUid, text, accessToken);
                assertEquals(savedCommentDto, result);
            }
        }

        verify(publicationProvider, times(1)).getPublication(publicationUUid);
        verify(commentRepo, times(1)).save(any(CommentDto.class));
        verify(userProvider, times(1)).getUser(accessToken);
    }

    @Test
    void updateTest() {
        var commentUuid = "OPVD34FERRE32233242EFHOIEWJFWIOE";
        var text = "Test comment";
        var accessToken = "efewrfewrfewfew";
        var userUuid = "111134FERRE32233242EFHOIEWJFWIOE";

        var commentsRepo = Mockito.mock(CommentsRepository.class);
        var userProvider = Mockito.mock(UserProvider.class);

        var service = new CommentsServiceImpl(commentsRepo, null, userProvider, null);

        var commentDto = CommentDto.builder()
                .id("1")
                .uuid(commentUuid)
                .text("First")
                .name("Aro Ani")
                .userUuid("TOPDSWEREWEFWEFWEF2323432EFWEFWE")
                .publicationUuid("VNJFODRTIU3456543VBJFOIRE567NJFD")
                .deleted(false)
                .createdAt(LocalDateTime.now())
                .deletedAt(null)
                .build();

        when(commentsRepo.getByUuidAndUserUuid(commentUuid, userUuid)).thenReturn(commentDto);

        var updatedCommentDto = CommentDto.builder()
                .id(commentDto.getId())
                .uuid(commentDto.getUuid())
                .text(text)
                .name(commentDto.getName())
                .userUuid(commentDto.getUserUuid())
                .publicationUuid(commentDto.getPublicationUuid())
                .deleted(commentDto.isDeleted())
                .createdAt(commentDto.getCreatedAt())
                .deletedAt(commentDto.getDeletedAt())
                .build();

        when(commentsRepo.save(any(CommentDto.class))).thenReturn(updatedCommentDto);

        var userDto = new UserDto();
        userDto.setUuid(userUuid);
        userDto.setFirstName("Nick");
        userDto.setLastName("Men");
        userDto.setMiddleName("Forever");
        var city = new UserDto.CityDto();
        city.setUuid("BOWBWEIFNWEFNWEOWJERIWEJROOEREWR");
        city.setName("Asgard");
        city.setRegion("Sky");
        city.setCountry("Valhalla");
        userDto.setCity(city);
        var skillDto = new UserDto.SkillDto();
        skillDto.setName("java");
        skillDto.setUuid("OIEBNFIWPEFNWEOIFNWEI2323434FGTR");
        skillDto.setLevel(10);
        userDto.setSkills(List.of(skillDto));
        userDto.setAvatarUrl("http://avatar.url");
        userDto.setAbout("one one");
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(50L);

        when(userProvider.getUser(accessToken)).thenReturn(userDto);

        service.update(commentUuid, text, accessToken);

        verify(commentsRepo, times(1)).getByUuidAndUserUuid(commentUuid, userUuid);
        verify(commentsRepo, times(1)).save(any(CommentDto.class));
        verify(userProvider, times(1)).getUser(accessToken);
    }

    @Test
    void updateTest_null() {
        var commentUuid = "OPVD34FERRE32233242EFHOIEWJFWIOE";
        var text = "Test comment";
        var accessToken = "efewrfewrfewfew";
        var userUuid = "111134FERRE32233242EFHOIEWJFWIOE";

        var commentsRepo = Mockito.mock(CommentsRepository.class);
        var userProvider = Mockito.mock(UserProvider.class);

        var service = new CommentsServiceImpl(commentsRepo, null, userProvider, null);

        when(commentsRepo.getByUuidAndUserUuid(commentUuid, userUuid)).thenReturn(null);

        var userDto = new UserDto();
        userDto.setUuid(userUuid);
        userDto.setFirstName("Nick");
        userDto.setLastName("Men");
        userDto.setMiddleName("Forever");
        var city = new UserDto.CityDto();
        city.setUuid("BOWBWEIFNWEFNWEOWJERIWEJROOEREWR");
        city.setName("Asgard");
        city.setRegion("Sky");
        city.setCountry("Valhalla");
        userDto.setCity(city);
        var skillDto = new UserDto.SkillDto();
        skillDto.setName("java");
        skillDto.setUuid("OIEBNFIWPEFNWEOIFNWEI2323434FGTR");
        skillDto.setLevel(10);
        userDto.setSkills(List.of(skillDto));
        userDto.setAvatarUrl("http://avatar.url");
        userDto.setAbout("one one");
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(50L);

        when(userProvider.getUser(accessToken)).thenReturn(userDto);

        assertThrows(CommentNotFoundException.class, () -> service.update(commentUuid, text, accessToken));

        verify(commentsRepo, times(1)).getByUuidAndUserUuid(commentUuid, userUuid);
        verify(userProvider, times(1)).getUser(accessToken);
    }

    @Test
    void deleteCommentByOwnerPublicationTest() {
        var commentUuid = "OPVD34FERRE32233242EFHOIEWJFWIOE";
        var publicationUuid = "111D34FERRE32233242EFHOIEWJFW222";
        var accessToken = "efewrfewrfewfew";

        var commentsRepo = Mockito.mock(CommentsRepository.class);
        var userProvider = Mockito.mock(UserProvider.class);
        var publicationProvider = Mockito.mock(PublicationProvider.class);

        var service = new CommentsServiceImpl(commentsRepo, publicationProvider, userProvider, null);

        var userDto = new UserDto();
        userDto.setUuid("CVGTYURERT5676543234567VDRTY76ZX");
        userDto.setFirstName("Nick");
        userDto.setLastName("Men");
        userDto.setMiddleName("Forever");
        var city = new UserDto.CityDto();
        city.setUuid("BOWBWEIFNWEFNWEOWJERIWEJROOEREWR");
        city.setName("Asgard");
        city.setRegion("Sky");
        city.setCountry("Valhalla");
        userDto.setCity(city);
        var skillDto = new UserDto.SkillDto();
        skillDto.setName("java");
        skillDto.setUuid("OIEBNFIWPEFNWEOIFNWEI2323434FGTR");
        skillDto.setLevel(10);
        userDto.setSkills(List.of(skillDto));
        userDto.setAvatarUrl("http://avatar.url");
        userDto.setAbout("one one");
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(50L);

        when(userProvider.getUser(accessToken)).thenReturn(userDto);

        var commentDto = CommentDto.builder()
                .id("1")
                .uuid(commentUuid)
                .text("Test comment")
                .name("Aro Ani")
                .userUuid("TOPDSWEREWEFWEFWEF2323432EFWEFWE")
                .publicationUuid("VNJFODRTIU3456543VBJFOIRE567NJFD")
                .deleted(false)
                .createdAt(LocalDateTime.now())
                .build();

        when(commentsRepo.getByUuid(commentUuid)).thenReturn(commentDto);

        var publication = new PublicationDto();
        publication.setUuid(publicationUuid);
        publication.setUserUuid(userDto.getUuid());
        publication.setTitle("Test title");
        publication.setDescription("Test text");
        publication.setCreatedAt(System.currentTimeMillis());
        publication.setStatus("ACTIVE");
        publication.setImageUrls(List.of("http://localhost:8045/img/HIHEIOFIEHSFS.jpg"));

        when(publicationProvider.getPublication(publicationUuid)).thenReturn(publication);

        var deletedCommentDto = CommentDto.builder()
                .id(commentDto.getId())
                .uuid(commentDto.getUuid())
                .text(commentDto.getText())
                .name(commentDto.getName())
                .userUuid(commentDto.getUserUuid())
                .publicationUuid(commentDto.getPublicationUuid())
                .deleted(true)
                .createdAt(commentDto.getCreatedAt())
                .build();

        when(commentsRepo.save(any(CommentDto.class))).thenReturn(deletedCommentDto);

        service.delete(commentUuid, publicationUuid, accessToken);

        verify(commentsRepo, times(1)).getByUuid(commentUuid);
        verify(commentsRepo, times(1)).save(any(CommentDto.class));
        verify(userProvider, times(1)).getUser(accessToken);
        verify(publicationProvider, times(1)).getPublication(publicationUuid);
    }

    @Test
    void deleteCommentByOwnerCommentTest() {
        var commentUuid = "OPVD34FERRE32233242EFHOIEWJFWIOE";
        var publicationUuid = "111D34FERRE32233242EFHOIEWJFW222";
        var accessToken = "efewrfewrfewfew";

        var commentsRepo = Mockito.mock(CommentsRepository.class);
        var userProvider = Mockito.mock(UserProvider.class);
        var publicationProvider = Mockito.mock(PublicationProvider.class);

        var service = new CommentsServiceImpl(commentsRepo, publicationProvider, userProvider, null);

        var userDto = new UserDto();
        userDto.setUuid("CVGTYURERT5676543234567VDRTY76ZX");
        userDto.setFirstName("Nick");
        userDto.setLastName("Men");
        userDto.setMiddleName("Forever");
        var city = new UserDto.CityDto();
        city.setUuid("BOWBWEIFNWEFNWEOWJERIWEJROOEREWR");
        city.setName("Asgard");
        city.setRegion("Sky");
        city.setCountry("Valhalla");
        userDto.setCity(city);
        var skillDto = new UserDto.SkillDto();
        skillDto.setName("java");
        skillDto.setUuid("OIEBNFIWPEFNWEOIFNWEI2323434FGTR");
        skillDto.setLevel(10);
        userDto.setSkills(List.of(skillDto));
        userDto.setAvatarUrl("http://avatar.url");
        userDto.setAbout("one one");
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(50L);

        when(userProvider.getUser(accessToken)).thenReturn(userDto);

        var commentDto = CommentDto.builder()
                .id("1")
                .uuid(commentUuid)
                .text("Test comment")
                .name("Aro Ani")
                .userUuid(userDto.getUuid())
                .publicationUuid(publicationUuid)
                .deleted(false)
                .createdAt(LocalDateTime.now())
                .build();

        when(commentsRepo.getByUuid(commentUuid)).thenReturn(commentDto);

        var deletedCommentDto = CommentDto.builder()
                .id(commentDto.getId())
                .uuid(commentDto.getUuid())
                .text(commentDto.getText())
                .name(commentDto.getName())
                .userUuid(commentDto.getUserUuid())
                .publicationUuid(commentDto.getPublicationUuid())
                .deleted(true)
                .createdAt(commentDto.getCreatedAt())
                .build();

        when(commentsRepo.save(any(CommentDto.class))).thenReturn(deletedCommentDto);

        service.delete(commentUuid, publicationUuid, accessToken);

        verify(commentsRepo, times(1)).getByUuid(commentUuid);
        verify(commentsRepo, times(1)).save(any(CommentDto.class));
        verify(userProvider, times(1)).getUser(accessToken);
    }

    @Test
    void deleteComment_NoPermissionsExceptionTest() {
        var commentUuid = "OPVD34FERRE32233242EFHOIEWJFWIOE";
        var publicationUuid = "111D34FERRE32233242EFHOIEWJFW222";
        var accessToken = "efewrfewrfewfew";

        var commentsRepo = Mockito.mock(CommentsRepository.class);
        var userProvider = Mockito.mock(UserProvider.class);
        var publicationProvider = Mockito.mock(PublicationProvider.class);

        var service = new CommentsServiceImpl(commentsRepo, publicationProvider, userProvider, null);

        var userDto = new UserDto();
        userDto.setUuid("CVGTYURERT5676543234567VDRTY76ZX");
        userDto.setFirstName("Nick");
        userDto.setLastName("Men");
        userDto.setMiddleName("Forever");
        var city = new UserDto.CityDto();
        city.setUuid("BOWBWEIFNWEFNWEOWJERIWEJROOEREWR");
        city.setName("Asgard");
        city.setRegion("Sky");
        city.setCountry("Valhalla");
        userDto.setCity(city);
        var skillDto = new UserDto.SkillDto();
        skillDto.setName("java");
        skillDto.setUuid("OIEBNFIWPEFNWEOIFNWEI2323434FGTR");
        skillDto.setLevel(10);
        userDto.setSkills(List.of(skillDto));
        userDto.setAvatarUrl("http://avatar.url");
        userDto.setAbout("one one");
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(50L);

        when(userProvider.getUser(accessToken)).thenReturn(userDto);

        var commentDto = CommentDto.builder()
                .id("1")
                .uuid(commentUuid)
                .text("Test comment")
                .name("Aro Ani")
                .userUuid("TOPDSWEREWEFWEFWEF2323432EFWEFWE")
                .publicationUuid("VNJFODRTIU3456543VBJFOIRE567NJFD")
                .deleted(false)
                .createdAt(LocalDateTime.now())
                .build();

        when(commentsRepo.getByUuid(commentUuid)).thenReturn(commentDto);

        var publication = new PublicationDto();
        publication.setUuid(publicationUuid);
        publication.setUserUuid("NVVNERVERVOIERVNVNEROIVNERO34R43");
        publication.setTitle("Test title");
        publication.setDescription("Test text");
        publication.setCreatedAt(System.currentTimeMillis());
        publication.setStatus("ACTIVE");
        publication.setImageUrls(List.of("http://localhost:8045/img/HIHEIOFIEHSFS.jpg"));

        when(publicationProvider.getPublication(publicationUuid)).thenReturn(publication);

        assertThrows(NoPermissionsException.class, () -> service.delete(commentUuid, publicationUuid, accessToken));

        verify(commentsRepo, times(1)).getByUuid(commentUuid);
        verify(commentsRepo, times(0)).save(any(CommentDto.class));
        verify(userProvider, times(1)).getUser(accessToken);
        verify(publicationProvider, times(1)).getPublication(publicationUuid);
    }

    @Test
    void deleteComment_NotFoundTest() {
        var commentUuid = "OPVD34FERRE32233242EFHOIEWJFWIOE";
        var publicationUuid = "111D34FERRE32233242EFHOIEWJFW222";
        var accessToken = "efewrfewrfewfew";

        var commentsRepo = Mockito.mock(CommentsRepository.class);
        var userProvider = Mockito.mock(UserProvider.class);
        var publicationProvider = Mockito.mock(PublicationProvider.class);

        var service = new CommentsServiceImpl(commentsRepo, publicationProvider, userProvider, null);

        var userDto = new UserDto();
        userDto.setUuid("CVGTYURERT5676543234567VDRTY76ZX");
        userDto.setFirstName("Nick");
        userDto.setLastName("Men");
        userDto.setMiddleName("Forever");
        var city = new UserDto.CityDto();
        city.setUuid("BOWBWEIFNWEFNWEOWJERIWEJROOEREWR");
        city.setName("Asgard");
        city.setRegion("Sky");
        city.setCountry("Valhalla");
        userDto.setCity(city);
        var skillDto = new UserDto.SkillDto();
        skillDto.setName("java");
        skillDto.setUuid("OIEBNFIWPEFNWEOIFNWEI2323434FGTR");
        skillDto.setLevel(10);
        userDto.setSkills(List.of(skillDto));
        userDto.setAvatarUrl("http://avatar.url");
        userDto.setAbout("one one");
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(50L);

        when(userProvider.getUser(accessToken)).thenReturn(userDto);
        when(commentsRepo.getByUuid(commentUuid)).thenReturn(null);

        assertThrows(CommentNotFoundException.class, () -> service.delete(commentUuid, publicationUuid, accessToken));

        verify(commentsRepo, times(1)).getByUuid(commentUuid);
        verify(commentsRepo, times(0)).save(any(CommentDto.class));
        verify(userProvider, times(1)).getUser(accessToken);
        verify(publicationProvider, times(0)).getPublication(publicationUuid);
    }

    @Test
    void deleteByUuidTest() {
        var commentUuid = "OPVD34FERRE32233242EFHOIEWJFWIOE";

        var commentsRepo = Mockito.mock(CommentsRepository.class);

        var service = new CommentsServiceImpl(commentsRepo, null, null, null);

        var commentDto = CommentDto.builder()
                .id("1")
                .uuid(commentUuid)
                .text("Test comment")
                .name("Aro Ani")
                .userUuid("TOPDSWEREWEFWEFWEF2323432EFWEFWE")
                .publicationUuid("VNJFODRTIU3456543VBJFOIRE567NJFD")
                .deleted(false)
                .createdAt(LocalDateTime.now())
                .deletedAt(null)
                .build();

        when(commentsRepo.getByUuid(commentUuid)).thenReturn(commentDto);

        var deletedCommentDto = CommentDto.builder()
                .id("1")
                .uuid(commentUuid)
                .text("Test comment")
                .name("Aro Ani")
                .userUuid("TOPDSWEREWEFWEFWEF2323432EFWEFWE")
                .publicationUuid("VNJFODRTIU3456543VBJFOIRE567NJFD")
                .deleted(true)
                .createdAt(commentDto.getCreatedAt())
                .deletedAt(null)
                .build();

        when(commentsRepo.save(any(CommentDto.class))).thenReturn(deletedCommentDto);

        service.delete(commentUuid);

        verify(commentsRepo, times(1)).getByUuid(commentUuid);
        verify(commentsRepo, times(1)).save(any(CommentDto.class));
    }

    @Test
    void deleteByUuidTest_null() {
        var commentUuid = "OPVD34FERRE32233242EFHOIEWJFWIOE";

        var commentsRepo = Mockito.mock(CommentsRepository.class);

        var service = new CommentsServiceImpl(commentsRepo, null, null, null);

        when(commentsRepo.getByUuid(commentUuid)).thenReturn(null);

        assertThrows(CommentNotFoundException.class, () -> service.delete(commentUuid));

        verify(commentsRepo, times(1)).getByUuid(commentUuid);
    }

    @Test
    void likeTest() {
        var commentUuid = "OPVD34FERRE32233242EFHOIEWJFWIOE";
        var accessToken = "efewrfewrfewfew";
        var userUuid = "111134FERRE32233242EFHOIEWJFWIOE";
        var time = System.currentTimeMillis();

        var commentsRepo = Mockito.mock(CommentsRepository.class);
        var userProvider = Mockito.mock(UserProvider.class);
        var producer = Mockito.mock(StatisticsProducer.class);

        var service = new CommentsServiceImpl(commentsRepo, null, userProvider, producer);

        var userDto = new UserDto();
        userDto.setUuid(userUuid);
        userDto.setFirstName("Nick");
        userDto.setLastName("Men");
        userDto.setMiddleName("Forever");
        var city = new UserDto.CityDto();
        city.setUuid("BOWBWEIFNWEFNWEOWJERIWEJROOEREWR");
        city.setName("Asgard");
        city.setRegion("Sky");
        city.setCountry("Valhalla");
        userDto.setCity(city);
        var skillDto = new UserDto.SkillDto();
        skillDto.setName("java");
        skillDto.setUuid("OIEBNFIWPEFNWEOIFNWEI2323434FGTR");
        skillDto.setLevel(10);
        userDto.setSkills(List.of(skillDto));
        userDto.setAvatarUrl("http://avatar.url");
        userDto.setAbout("one one");
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(50L);

        when(userProvider.getUser(accessToken)).thenReturn(userDto);

        var commentLikeDto = CommentLikeDto.builder()
                .objectUuid(commentUuid)
                .userUuid(userUuid)
                .createdAt(time)
                .build();

        doNothing().when(producer).likeComment(eq(commentLikeDto));
        when(commentsRepo.isExists(commentUuid)).thenReturn(true);

        try (MockedStatic<DateUtil> dateUtil = Mockito.mockStatic(DateUtil.class)) {
            dateUtil.when(DateUtil::getTimeNowMillisUTC).thenReturn(time);
            service.like(commentUuid, accessToken);
        }

        verify(userProvider, times(1)).getUser(accessToken);
        verify(commentsRepo, times(1)).isExists(commentUuid);
        verify(producer, times(1)).likeComment(any(CommentLikeDto.class));
    }

    @Test
    void like_notFoundTest() {
        var commentUuid = "OPVD34FERRE32233242EFHOIEWJFWIOE";
        var accessToken = "efewrfewrfewfew";
        var userUuid = "111134FERRE32233242EFHOIEWJFWIOE";

        var commentsRepo = Mockito.mock(CommentsRepository.class);
        var userProvider = Mockito.mock(UserProvider.class);
        var producer = Mockito.mock(StatisticsProducer.class);

        var service = new CommentsServiceImpl(commentsRepo, null, userProvider, producer);

        var userDto = new UserDto();
        userDto.setUuid(userUuid);
        userDto.setFirstName("Nick");
        userDto.setLastName("Men");
        userDto.setMiddleName("Forever");
        var city = new UserDto.CityDto();
        city.setUuid("BOWBWEIFNWEFNWEOWJERIWEJROOEREWR");
        city.setName("Asgard");
        city.setRegion("Sky");
        city.setCountry("Valhalla");
        userDto.setCity(city);
        var skillDto = new UserDto.SkillDto();
        skillDto.setName("java");
        skillDto.setUuid("OIEBNFIWPEFNWEOIFNWEI2323434FGTR");
        skillDto.setLevel(10);
        userDto.setSkills(List.of(skillDto));
        userDto.setAvatarUrl("http://avatar.url");
        userDto.setAbout("one one");
        userDto.setStatus("ACTIVE");
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(50L);

        when(userProvider.getUser(accessToken)).thenReturn(userDto);

        when(commentsRepo.isExists(commentUuid)).thenReturn(false);

        assertThrows(CommentNotFoundException.class, () -> service.like(commentUuid, accessToken));

        verify(userProvider, times(1)).getUser(accessToken);
        verify(commentsRepo, times(1)).isExists(commentUuid);
        verify(producer, times(0)).likeComment(any(CommentLikeDto.class));
    }
}