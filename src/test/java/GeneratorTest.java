import com.autum.comments.utils.Generator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class GeneratorTest {

    @Test
    void generateUUIDTest() {

        var uuid = Generator.generateUUID();
        assertNotNull(uuid);
        assertEquals(32, uuid.length());
        assertFalse(uuid.contains("-"));
        assertFalse(uuid.contains(" "));
    }
}