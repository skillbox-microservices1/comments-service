package api.handler;

import com.autum.comments.api.error.handler.CustomExceptionHandler;
import com.autum.comments.business.comments.exception.CommentNotFoundException;
import com.autum.comments.business.comments.exception.NoPermissionsException;
import com.autum.comments.business.provider.ProviderException;
import org.junit.jupiter.api.Test;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;


import java.util.Locale;

import static com.autum.comments.api.error.ErrorCode.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;


public class CustomExceptionHandlerTest {

    @Test
    void commentNotFound() {
        var exception = new CommentNotFoundException();
        var locale = new Locale("en");
        var msg = "This comment was not found";
        var errorCode = COMMENT_NOT_FOUND;

        var messageSource = mock(MessageSource.class);
        var exceptionHandler = new CustomExceptionHandler(messageSource);

        when(messageSource.getMessage(errorCode.name(), null, locale)).thenReturn(msg);

        var result = exceptionHandler.commentNotFound(exception, locale);

        assertEquals(errorCode, result.getErrorCode());
        assertEquals(msg, result.getMessage());
        assertNull(result.getDetails());

        verify(messageSource, times(1)).getMessage(errorCode.name(), null, locale);
    }

    @Test
    void notPermission() {
        var exception = new NoPermissionsException("TEST");
        var locale = new Locale("en");
        var msg = "The role does not match, access is denied";
        var errorCode = ACCESS_DENIED;

        var messageSource = mock(MessageSource.class);
        var exceptionHandler = new CustomExceptionHandler(messageSource);

        when(messageSource.getMessage(errorCode.name(), null, locale)).thenReturn(msg);

        var result = exceptionHandler.notPermission(exception, locale);

        assertEquals(errorCode, result.getErrorCode());
        assertEquals(msg, result.getMessage());
        assertNull(result.getDetails());

        verify(messageSource, times(1)).getMessage(errorCode.name(), null, locale);
    }

    @Test
    void accessDenied() {
        var exception = new AccessDeniedException(null, null);
        var locale = new Locale("en");
        var msg = "The role does not match, access is denied";
        var errorCode = ACCESS_DENIED;

        var messageSource = mock(MessageSource.class);
        var exceptionHandler = new CustomExceptionHandler(messageSource);

        when(messageSource.getMessage(errorCode.name(), null, locale)).thenReturn(msg);

        var result = exceptionHandler.accessDenied(exception, locale);

        assertEquals(errorCode, result.getErrorCode());
        assertEquals(msg, result.getMessage());
        assertNull(result.getDetails());

        verify(messageSource, times(1)).getMessage(errorCode.name(), null, locale);
    }

    @Test
    void testInternalError() {
        var exception = new Throwable("TEST EXCEPTION MSG");
        var locale = new Locale("en");
        var msg = "Internal server error";
        var errorCode = INTERNAL_ERROR;

        var messageSource = mock(MessageSource.class);
        var exceptionHandler = new CustomExceptionHandler(messageSource);

        when(messageSource.getMessage(errorCode.name(), null, locale)).thenReturn(msg);

        var result = exceptionHandler.internalError(exception, locale);

        assertEquals(errorCode, result.getErrorCode());
        assertEquals(msg, result.getMessage());
        assertNull(result.getDetails());

        verify(messageSource, times(1)).getMessage(errorCode.name(), null, locale);
    }

    @Test
    void providerException_unexpectedException() {
        var body = "TEST EXCEPTION MSG";
        var status = HttpStatus.BAD_REQUEST;
        var exception = new ProviderException(status.value(), body);

        var messageSource = mock(MessageSource.class);
        var exceptionHandler = new CustomExceptionHandler(messageSource);

        var result = exceptionHandler.providerException(exception);

        assertEquals(body, result.getBody());
        assertEquals(status, result.getStatusCode());
    }

    @Test
    void providerException_httpMediaTypeNotAcceptableException() {
        var exception = new HttpMediaTypeNotAcceptableException("TEST");

        var locale = new Locale("en");
        var msg = "Incorrect media type";
        var errorCode = INVALID_MEDIA_TYPE;

        var messageSource = mock(MessageSource.class);
        var exceptionHandler = new CustomExceptionHandler(messageSource);

        when(messageSource.getMessage(errorCode.name(), null, locale)).thenReturn(msg);

        var result = exceptionHandler.handleHttpMediaTypeNotAcceptableException(exception, locale);
        assertEquals(errorCode, result.getErrorCode());
        assertEquals(msg, result.getMessage());
        assertNull(result.getDetails());

        verify(messageSource, times(1)).getMessage(errorCode.name(), null, locale);
    }
}