package api.conroller;

import com.autum.comments.api.controller.SharedRoleController;
import com.autum.comments.api.model.response.CommentListResponse;
import com.autum.comments.api.model.response.CommentResponse;
import com.autum.comments.business.comments.CommentsServiceImpl;
import com.autum.comments.business.comments.dto.CommentDto;
import com.autum.comments.business.comments.dto.CommentListDto;
import com.autum.comments.infrastructure.mapstruct.Mapper;
import com.autum.comments.utils.DateUtil;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


public class SharedRoleControllerTest {

    @Test
    void getListTest() {
        var uuid = "YTEE43FEWFWEF23423432FEFEWF45654";
        var pageable = Pageable.ofSize(10);

        var commentsService = Mockito.mock(CommentsServiceImpl.class);
        var mapper = Mockito.mock(Mapper.class);
        var controller = new SharedRoleController(commentsService, mapper);

        var commentDto = CommentDto.builder()
                .id("1")
                .uuid("OPMNVFDRTEOWWE2332MOEFOI332VFGTR")
                .text("Test comment")
                .name("Aro Ani")
                .userUuid("TOPDSWEREWEFWEFWEF2323432EFWEFWE")
                .publicationUuid(uuid)
                .deleted(false)
                .createdAt(LocalDateTime.now())
                .deletedAt(null)
                .build();

        var commentListDto = CommentListDto.builder()
                .totalCount(1)
                .comments(List.of(commentDto))
                .build();

        var commentResponse = new CommentResponse();
        commentResponse.setUuid(commentDto.getUuid());
        commentResponse.setText(commentDto.getText());
        commentResponse.setName(commentDto.getName());
        commentResponse.setUserUuid(commentDto.getUserUuid());
        commentResponse.setCreatedAt(DateUtil.getMillisFromLocalDateTime(commentDto.getCreatedAt()));

        var commentListResponse = List.of(commentResponse);

        var commentsResponse = new CommentListResponse();
        commentsResponse.setTotalCount(commentListDto.getTotalCount());
        commentsResponse.setItems(commentListResponse);

        when(commentsService.getCommentList(uuid, pageable)).thenReturn(commentListDto);
        when(mapper.map(commentListDto, CommentListResponse.class)).thenReturn(commentsResponse);

        var result = controller.getList(uuid, pageable);
        assertEquals(1, result.getTotalCount());
        assertNotNull(result.getItems());
        assertEquals(1, result.getItems().size());

        var commentResult = result.getItems().get(0);
        assertEquals(commentResponse.getUuid(), commentResult.getUuid());
        assertEquals(commentResponse.getText(), commentResult.getText());
        assertEquals(commentResponse.getName(), commentResult.getName());
        assertEquals(commentResponse.getUserUuid(), commentResult.getUserUuid());
        assertEquals(commentResponse.getCreatedAt(), commentResult.getCreatedAt());

        verify(commentsService, times(1)).getCommentList(uuid, pageable);
        verify(mapper, times(1)).map(commentListDto, CommentListResponse.class);
    }
}