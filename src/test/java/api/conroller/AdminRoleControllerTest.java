package api.conroller;

import com.autum.comments.api.controller.AdminRoleController;
import com.autum.comments.business.comments.CommentsServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;


public class AdminRoleControllerTest {


    @Test
    void deleteTest() {
        var uuid = "KOLFD45324NBMCBNF332432GEGRESS54";

        var commentsService = Mockito.mock(CommentsServiceImpl.class);
        var controller = new AdminRoleController(commentsService);

        doNothing().when(commentsService).delete(uuid);

        controller.delete(uuid);

        verify(commentsService, times(1)).delete(uuid);
    }
}